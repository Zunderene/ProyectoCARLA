import random
import Modulos.ClasificadorSeñales as CL
import SystemaFuzzy.Controlador as controladorFuzzy
from carla import image_converter
from carla.sensor import Camera
from carla.settings import CarlaSettings
from carla.client import make_carla_client, VehicleControl
import Modulos.MantenerseCarril as ma
import cv2
import numpy as np

WINDOW_WIDTH = 800
WINDOW_HEIGHT = 600
MINI_WINDOW_WIDTH = 320
MINI_WINDOW_HEIGHT = 180
MINI_WINDOW_WIDTH2 = 640
MINI_WINDOW_HEIGHT2 = 180


class running(object):
    velocidades = [None, 35, 35, -100, 35, 60, 90]

    def __init__(self):
        self._carla_settings = self.cargarConfig()
        self.Aceleracion = controladorFuzzy.Controlador('Aceleracion', 'dataacel.xlsx', 'FuzzySystem')
        self.Giro = controladorFuzzy.Controlador('Giro', 'dataacel.xlsx', 'FuzzySystem')



    def run(self):
     velocidadGLOBLA = 30
     deifVel = 0
     reduccion = 0
     vargiro = 0.0
     other_lane=0.0
     offroad = 0.0
     iter = 0
     giroValor = 0
     decsiones = []
     plan = 0
     with make_carla_client('localhost', 2000) as client:
         print("Empieza....")
         scene = client.load_settings(self._carla_settings)
         client.start_episode(30) #13,12,25,2

         while True:
             measurements, sensor_data = client.read_data()
             imagenGiro = sensor_data.get('carrilcamara', None)
             imagenSignalRGB = sensor_data.get('signalCamaraRGB', None)
             imagenSignal = sensor_data.get('signalCamara', None)
             imagenPlano = sensor_data.get('plano', None)

             control = VehicleControl()
             control.steer = 0

             if(imagenSignalRGB is not None and imagenSignal is not None and imagenGiro is not None and imagenPlano is not None):

                 CLA = CL.ClassificadorSeñales(imagenSignalRGB, imagenSignal, 1)
                 valorLimite = CLA.salida()
                 if valorLimite is not None and valorLimite > 0:
                    velocidadGLOBLA = self.velocidades[valorLimite]

            
                 imagenGiro = image_converter.labels_to_cityscapes_palette(imagenGiro)
                 #imagenGiro = cv2.cvtColor(imagenGiro, cv2.COLOR_BGR2RGB)

                 imagenSegaux = imagenGiro[:, :, 0].copy()
                 imagenGiro[:, :, 0] = imagenGiro[:, :, 2].copy()
                 imagenGiro[:, :, 2] = imagenSegaux

                 ma.setPlanificador(plan)
                 Giro_modulo = ma.situacion(imagenGiro)


                 other_lane = 100 * measurements.player_measurements.intersection_otherlane
                 offroad = 100 * measurements.player_measurements.intersection_offroad
                 giroValor = Giro_modulo[0]
                 if (Giro_modulo[1] == 2 or Giro_modulo[1] == -2):
                     reduccion = 20
                     other_lane = other_lane - 15
                     decsiones = []
                 elif (Giro_modulo[1] == 1 or Giro_modulo[1] == -1):
                     reduccion = 2
                     other_lane = other_lane - 15
                 else:
                     reduccion = 0
                     viasLibre = self.posiciones(image_converter.labels_to_cityscapes_palette(imagenPlano))

                     if(viasLibre[1] >= 30 and not -1 in decsiones):
                         if (viasLibre[0] >= 90):
                             decsiones = np.append(decsiones, 0)
                         decsiones = np.append(decsiones,-1)
                     if (viasLibre[2] >= 30 and not  1 in decsiones ):
                         if (viasLibre[0] >= 90 and viasLibre[1] < 15 ):
                            decsiones = np.append(decsiones, 0)
                         decsiones = np.append(decsiones, 1)

                     if(len(decsiones) > 0 ):
                         plan = random.choice(decsiones)
                     print(decsiones)



             vargiro = self.Giro.calcularY2([offroad, other_lane - 10, giroValor])

             velocidadDeseada = (velocidadGLOBLA-reduccion) - measurements.player_measurements.forward_speed * 3.6
             var = self.Aceleracion.calcularY0([-velocidadDeseada, (deifVel - measurements.player_measurements.forward_speed * 3.6)])

             if var > 0:
                 control.throttle = var
             elif var <= 0:
                 control.brake = -var

             control.steer = vargiro
             deifVel = measurements.player_measurements.forward_speed * 3.6
             client.send_control(control)
             iter = iter + 1




    def posiciones(self, img):
        puntoX = int(len(img)/2)
        puntoY = int(len(img[0])/2)-3
        contador = [0, 0, 0]
        puntoX = puntoX - 30

        while(contador[0] <  int(len(img)/2) and img[puntoX - contador[0], puntoY][2] == 128):
            contador[0] = contador[0] + 1

        while (contador[1] < int(len(img[0])/2)-3 and img[puntoX, puntoY - contador[1] ][2] == 128):
            contador[1] = contador[1] + 1

        while (contador[2] < int(len(img[0])/2)-3 and img[puntoX, contador[2] + puntoY][2] == 128):
            contador[2] = contador[2] + 1

        img[puntoX:puntoX+2, puntoY:puntoY+2] = [6, 57, 113]
        cv2.imwrite('./' + str(iter) + '.tif', img)
        print(contador)
        return contador




    def cargarConfig(self):
        carlaSettings = CarlaSettings()
        carlaSettings.set(
            SynchronousMode=True,
            SendNonPlayerAgentsInfo=True,
            NumberOfVehicles=0,
            NumberOfPedestrians=10,
            WeatherId=random.choice([1, 3, 7, 8, 14]),
            QualityLevel='Low')

        signalcamara = Camera('signalCamara', PostProcessing='SemanticSegmentation')
        signalcamara.set_image_size(WINDOW_WIDTH, WINDOW_HEIGHT)
        signalcamara.set_position(2.0, -0.5, 1.4)
        signalcamara.set_rotation(0.0, 0.0, 0.0)
        carlaSettings.add_sensor(signalcamara)
        signalcamaraRGB = Camera('signalCamaraRGB')
        signalcamaraRGB.set_image_size(WINDOW_WIDTH, WINDOW_HEIGHT)
        signalcamaraRGB.set_position(2.0, -0.5, 1.4)
        signalcamaraRGB.set_rotation(0.0, 0.0, 0.0)
        carlaSettings.add_sensor(signalcamaraRGB)
        carrilcamara = Camera('carrilcamara', PostProcessing='SemanticSegmentation')
        carrilcamara.set_image_size(MINI_WINDOW_WIDTH2, MINI_WINDOW_HEIGHT2)
        carrilcamara.set_position(0.0, 0.0, 2.0)
        carrilcamara.set_rotation(-10.0, 0.0, 0.0)
        carrilcamara.FOV = 120
        carlaSettings.add_sensor(carrilcamara)
        camaraplano = Camera('plano', PostProcessing='SemanticSegmentation')
        camaraplano.set_image_size(MINI_WINDOW_WIDTH, MINI_WINDOW_HEIGHT)
        camaraplano.set_position(0.0, 0.0, 100.0)
        camaraplano.set_rotation(-90.0, 0.0, 0.0)
        carlaSettings.add_sensor(camaraplano)
        return carlaSettings

if __name__ == '__main__':

    try:
        RUN = running()
        RUN.run()
    except KeyboardInterrupt:
        print('\nCancelled by user. Bye!')