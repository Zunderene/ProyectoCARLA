import cv2
import numpy as np

from carla import image_converter
from sklearn.externals import joblib

class ClassificadorSeñales(object):
    rangos = [[220, 220, 0], [220, 220, 225]]
    Nombre = ["Ruido", "SemaforosVerdes", "SemaforosAmarillos", "SemaforosRojos", "Senal30", "Senal60", "Senal90", ]
    NombreRed = "Modulo_Detencion_Trafico.sav"

    def __init__(self, imagenRGB, imagenSeg, iter):
        self.redNN = None
        self.x_test = None
        self.imagenRGB = image_converter.to_rgb_array(imagenRGB)
        self.imagenSeg = image_converter.labels_to_cityscapes_palette(imagenSeg)

        self.imagenRGB = self.imagenRGB[100:150 + len(self.imagenRGB) - 150, 300:350 + (len(self.imagenRGB[0]) - 350)]
        self.imagenSeg = self.imagenSeg = self.imagenSeg[100:150 + len(self.imagenSeg) - 150, 300:350 + (len(self.imagenSeg[0]) - 350)]

        #cv2.imwrite('./' + str(iter) + '.tif', self.imagenRGB)
        #cv2.imwrite('./' + str(iter) + 'seg.tif', self.imagenSeg)

        mascara = cv2.inRange(self.imagenSeg, np.array(self.rangos[0], dtype=np.uint8), np.array(self.rangos[1], dtype=np.uint8))
        mascara, contorno, _ = cv2.findContours(mascara, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        max_area = 0
        best_cnt = None

        for cnt in contorno:
            area = cv2.contourArea(cnt)
            if area > max_area:
                best_cnt = cnt
                max_area = area

        if max_area > 300:
            (x, y, w, h) = cv2.boundingRect(best_cnt)
            res = cv2.bitwise_or(self.imagenRGB, self.imagenRGB, mask=mascara)
            img = res[y:h + y, x:x + w]
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

            ImagenTest = self.tratamiento(img)
            self.redNN = joblib.load(self.NombreRed)
            self.x_test = [self.obtencion(ImagenTest)]



    def tratamiento(self, Imagen):
        Imagen = cv2.cvtColor(Imagen, cv2.COLOR_RGB2GRAY)
        Imagen = cv2.resize(Imagen, (41, 41))
        return Imagen

    def obtencion(self, Imagen):
        X = []
        for ejeX in range(0, len(Imagen)):
            for ejeY in range(0, len(Imagen[ejeX])):
                X.append(Imagen[ejeX, ejeY])
        return X

    def salida(self):
        if(self.x_test is not None and self.redNN is not None):
            return self.redNN.predict(self.x_test)[0]
        else:
            return None