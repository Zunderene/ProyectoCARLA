import openpyxl as xml
from SystemaFuzzy import Etiqueta as Eti, Regla as Reg
import numpy as np

class Controlador(object):
    reglas = None
    antecedente01 = None
    antecedente02 = None
    antecedente03 = None
    antecedente04 = None
    consecuente = None
    CV = None
    CR = None

    def __init__(self, nombre, filename, hoja):
        self.nombre = nombre
        self.filename = filename
        self.hoja = hoja
        self.doc = xml.load_workbook(self.filename)
        self.leerConfiguracion(self.hoja)
        #print(self.calcularY0([10,10]))

    def leerConfiguracion(self, nombre):
        hoja = self.doc[nombre]

        if self.nombre == 'Aceleracion':
            self.antecedente01 = self.leerEtiqueta(hoja, 10, 14)
            self.antecedente02 = self.leerEtiqueta(hoja, 17, 21)
            self.consecuente = self.leerEtiqueta(hoja, 2, 7)
            self.leerReglas_3('Aceleracion')

        elif self.nombre == 'Giro':
            self.antecedente01 = self.leerEtiqueta(hoja, 24, 25)
            self.antecedente02 = self.leerEtiqueta(hoja, 24, 25)
            self.antecedente03 = self.leerEtiqueta(hoja, 28, 34)
            #self.antecedente04 = self.leerEtiqueta(hoja, 42, 48)
            self.consecuente = self.leerEtiqueta(hoja, 42, 48)
            self.leerReglas_5('Giro')

    def leerEtiqueta(self, hoja, filainicio, filafinal):
        Conjunot_etiqueta = {}
        filaExcel = filainicio

        while(filaExcel <= filafinal):
            nombre = hoja['A' + str(filaExcel)].value
            eti = Eti.Etiqueta(nombre, hoja['B' + str(filaExcel)].value, hoja['C' + str(filaExcel)].value, hoja['D' + str(filaExcel)].value)
            Conjunot_etiqueta[nombre] = eti
            filaExcel += 1

        return Conjunot_etiqueta

    def leerReglas_3(self,tregla):
        hoja = self.doc[tregla]
        self.CR = []
        ind = 1
        for row in hoja.iter_rows(min_row=2, max_col=3):
            reg = Reg.Regla(ind, [row[0].value, row[1].value, row[2].value])
            ind += 1
            self.CR.append(reg)

    def leerReglas_5(self, tregla):
        hoja = self.doc[tregla]
        self.CR = []
        ind = 1
        for row in hoja.iter_rows(min_row=2, max_col=5):
            reg = Reg.Regla(ind,[row[0].value, row[1].value, row[2].value,  row[3].value])
            ind += 1
            self.CR.append(reg)


    def calcularY0(self, entradas):
        hmin = []
        xyt = []
        xyt2 = []
        for regla in self.CR:
            ent1 = self.antecedente01[regla.argumentos()[0]]
            ent2 = self.antecedente02[regla.argumentos()[1]]
            cons = self.consecuente[regla.consecuente()]

            h1 = self.calculoEmparejamiento(ent1, entradas[0])
            h2 = self.calculoEmparejamiento(ent2, entradas[1])

            if(h1 != 0) and (h2 != 0):
                hmin.append(np.min([h1, h2]))
                name, x0, x1, x2 = cons.getEtiqueta()
                #print("Regla activa: " + str(name) + " Con la regla " + str(regla.id) + " " + str(h1) + " " + str(h2))
                valor = x0 + (x1 - x0) * np.min([h1, h2])
                xyt.append(valor)
                valor = x2 - (x2 - x1) * np.min([h1, h2])
                xyt2.append(valor)

        return self.deffuzy(hmin, xyt, xyt2)

    def calcularY2(self, entradas):
        hmin = []
        xyt = []
        xyt2 = []

        for regla in self.CR:
            ent1 = self.antecedente01[regla.argumentos()[0]]
            ent2 = self.antecedente02[regla.argumentos()[1]]
            ent3 = self.antecedente03[regla.argumentos()[2]]
            #ent4 = self.antecedente04[regla.argumentos()[3]]
            cons = self.consecuente[regla.consecuente()]

            h1 = self.calculoEmparejamiento2(ent1, entradas[0])
            h2 = self.calculoEmparejamiento2(ent2, entradas[1])
            h3 = self.calculoEmparejamiento2(ent3, entradas[2])
            #h4 = self.calculoEmparejamiento2(ent4, entradas[3])

            if(h1 != 0) and (h2 != 0) and (h3 != 0):
                hmin.append(np.min([h1, h2, h3]))
                name, x0, x1, x2 = cons.getEtiqueta()
                print("Regla activa: " + str(name) + " Con la regla " + str(regla.id) + " " + str(h1) + " " + str(h2))
                valor = x0 + (x1 - x0) * np.min([h1, h2, h3])
                xyt.append(valor)
                valor = x2 - (x2 - x1) * np.min([h1, h2, h3])
                xyt2.append(valor)

        return self.deffuzy(hmin, xyt, xyt2)

    def calculoEmparejamiento(self, eti, ej):
        name, x0, x1, x2 = eti. getEtiqueta()

        if name == 'muy_lento' and ej <= x0:
            return 1.0
        elif name == 'muy_rapido' and ej >= x2:
            return 1.0

        if ej <= x0:
            return 0.0
        elif (x0 <= ej) and (ej <= x1):
            return (ej - x0) / (x1 - x0)
        elif (x1 <= ej) and (ej <= x2):
            return (x2 - ej) / (x2 - x1)
        elif x2 <= ej:
            return 0.0
    def calculoEmparejamiento2(self, eti, ej):
        name, x0, x1, x2 = eti. getEtiqueta()


        if name == 'muy_salido' and ej >= x2:
            return 1.0
        elif name == 'normal' and ej <= x0:
            return 1.0

        elif name == 'muy_Derecha' and ej >= x2:
            return 1.0
        elif name == 'muy_Izquierda' and ej <= x0:
            return 1.0
        elif name == 'rapido' and ej >= x2:
            return 1.0
        elif name == 'lento' and ej <= x0:
            return 1.0

        if ej <= x0:
            return 0.0
        elif (x0 <= ej) and (ej <= x1):
            return (ej - x0) / (x1 - x0)
        elif (x1 <= ej) and (ej <= x2):
            return (x2 - ej) / (x2 - x1)
        elif x2 <= ej:
            return 0.0


    def deffuzy(self, hmin, xyt, xyt2):
        numerador = 0
        denominador = 0

        for index in range(0, len(hmin)):
            numerador = numerador + hmin[index]*((xyt[index] + xyt2[index])/2)
            denominador = denominador + hmin[index]
        if denominador != 0:
            return numerador / denominador
        else:
            return 0

    def mostrarVariables(self):
        None

    def mostrarReglas(self):
        None

if __name__ == "__main__":
    C = Controlador('Aceleracion', 'dataacel.xlsx', 'FuzzySystem')
    C2 = Controlador('Giro', 'dataacel.xlsx', 'FuzzySystem')