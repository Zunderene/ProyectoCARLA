

class Etiqueta(object):

    def __init__(self, nombre, x0, x1, x2):
        self.nombre = nombre
        self.x0 = x0
        self.x1 = x1
        self.x2 = x2

    def getEtiqueta(self):
        return self.nombre, self.x0, self.x1, self.x2

    def __str__(self):
        return 'Nombre: ' + str(self.nombre) + 'X0: ' + str(self.x0) + 'X1: ' + str(self.x1) + 'X2: ' + str(self.x2)