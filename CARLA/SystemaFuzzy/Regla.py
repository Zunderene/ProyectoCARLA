
class Regla(object):
    id = None
    ArgumentoLista = None

    def __init__(self,id, listaArg):
        self.id = id
        self.ArgumentoLista = listaArg


    def argumentos(self):
        return self.ArgumentoLista

    def consecuente(self):
        return self.ArgumentoLista[len(self.ArgumentoLista) - 1]

    def id(self):
        return self.id